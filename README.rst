restfull todo app
=================

This application implements the todo manager introduced by Jeremy Howard at 
http://jphoward.wordpress.com/2013/01/04/end-to-end-web-app-in-under-an-hour/


The todo manager allows to add, modify and delete task with a title, a due date 
and a priority. In addition to Jeremy's proposal, the software is extended with 
a user management. A user can only works on his own tasks. Users can be manipulated 
by admins. The database is initialized with one admin (password:admin) and one 
standard user (password: user) by the commmand 'runserver.py --create'.

Implementation
**************
The software is a todo web application, that shows how to use a RESTfull 
server-side API to a database and it's connection to a JavaSCript-based 
web-client, using AngularJS and bootstrap.

The server is using **Flask** web-framework and a sqlite database. The scripts 
around the software are ready to run the server within the **heroku** environment 
(foreman etc.).

The web-client is implemented in Javascript and uses **AngularJS** to connect 
the server-communication with CRUD controllers and HTML-views.


Feel free to contact me (Bastian Migge <bastian@migge.info>) for any suggestions or
questions. I'd be happy if you want to contribute to this little project.


