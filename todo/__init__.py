# -*- coding: utf-8 -*-
"""
    todo
    ~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: core web app
"""

import flask
import flask.ext.sqlalchemy
import flask.ext.restless
import todo.models
import todo.database
from todo.logger import logger
from todo.rest import check_login
from todo.rest import delete_user, get_many_user, get_single_user, \
    patch_single_user, post_user
from todo.rest import delete_todo, get_many_todo, get_single_todo, \
    patch_single_todo, post_todo

logger.debug('application started')


# Create the Flask application and the Flask-SQLAlchemy object.
app = flask.Flask(__name__)
app.config.from_object('todo.config')

# Create the Flask-Restless API manager.
manager = flask.ext.restless.APIManager(app, session=database.db_session)

manager.create_api(todo.models.TodoItem,
                   methods=['GET', 'POST', 'DELETE', 'PUT'],
                   results_per_page=20,
                   preprocessors={
                        'GET_SINGLE'   : [check_login, get_single_todo],
                        'GET_MANY'     : [check_login, get_many_todo],
                        'PATCH_SINGLE' : [check_login, patch_single_todo],
                        'POST'         : [check_login, post_todo],
                        'DELETE'       : [check_login, delete_todo]
                   }
                   )

manager.create_api(todo.models.User,
                   methods=['GET', 'POST', 'DELETE', 'PUT'],
                   results_per_page=20,
                   preprocessors={
                        'GET_SINGLE'   : [check_login, get_single_user],
                        'GET_MANY'     : [check_login, get_many_user],
                        'PATCH_SINGLE' : [check_login, patch_single_user],
                        'POST'         : [check_login, post_user],
                        'DELETE'       : [check_login, delete_user]
                   }
)


import todo.views

@app.teardown_request
def shutdown_session(exception=None):
    database.db_session.remove()

@app.before_request
def before_request():
    pass


