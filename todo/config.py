# -*- coding: utf-8 -*-
"""
    todo.config
    ~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: config file
"""

# Flask 
DEBUG = True
SECRET_KEY = 'no_secret'

# database
SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/todo.db'
