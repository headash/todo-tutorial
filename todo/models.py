# -*- coding: utf-8 -*-
"""
    todo.models
    ~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: models
"""

import datetime 
import flask.ext.sqlalchemy
from sqlalchemy import Column, Integer, SmallInteger, Boolean, Date, Unicode, \
    ForeignKey
from sqlalchemy.orm import relationship, backref
from todo.database import Base, db_session

class TodoItem(Base):
    __tablename__ = 'todoitem'
    id       = Column(Integer, primary_key=True)
    todo     = Column(Unicode)
    priority = Column(SmallInteger)
    due_date = Column(Date)
    done     = Column(Boolean)
    owner_id = Column(Integer, ForeignKey('user.id'))
    owner    = relationship('User', backref=backref('projects', lazy='dynamic'))


    def __init__(self, todo, priority, owner_id, due_date = None, done = None):
        self.todo     = todo
        self.priority = priority
        self.owner_id = owner_id
        self.due_date = due_date or datetime.date.today()
        self.done     = done or False


class User(Base):
    __tablename__ = 'user'

    id          = Column(Integer, primary_key=True)
    email       = Column(Unicode, unique=True)
    password    = Column(Unicode)
    admin       = Column(Boolean)

    def __init__(self, email, password, admin=None):
        self.email    = email
        self.password = password
        self.admin    = admin or False

    def __repr__(self):
        asdict =  { 'id' : self.id,
                 'email' : self.email,
                 'password' : self.password,
                 'admin' : self.admin
        }
        return '<%s %s>' %(self.__class__.__name__, asdict)
